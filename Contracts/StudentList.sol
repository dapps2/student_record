// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract StudentList{
    uint public studentsCount = 1;

    struct Student {
        uint _id;
        uint cid;
        string name;
        bool graduated;
    }
    mapping(uint => Student) public students;

    
    constructor() {
        // createStudent(1001, "Bijay Kumar Rai");
    }

    function createStudent(uint _studentCid, string memory _name)
    public returns (Student memory){
        studentsCount++;
        students[studentsCount] = Student(studentsCount, _studentCid, _name, false);
        emit createStudentEvent(studentsCount, _studentCid, _name, false);
        return students[studentsCount];
    }

    event createStudentEvent(
        uint _id,
        uint indexed cid,
        string name,
        bool graduated
    );

    //Event for gratuation status
    event markGraduatedEvent(
        uint indexed cid
    );
    //Change graduation status of student
    function markGraduated(uint _id)public returns (Student memory){
        students[_id].graduated = true;
        //trigger create event
        emit markGraduatedEvent(_id);
        return students[_id];
    }
    //Fetch students info from storage 
    function findStudent(uint _id) public view returns (Student memory){
        return students[_id];
    }
   event eventUPdateName(uint id);
   function updateStudent(uint _id, string memory _name) public returns(Student memory){
    students[_id].name=_name;
    emit eventUPdateName(_id);
    return students[_id];
   }
}