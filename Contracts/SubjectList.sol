//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
contract SubjectList{
    uint public subjectsCount = 0;

    // constructor(){
    //     createSubject("CSB202", "programming to blockchain");
    // }

    struct Subject{
        uint _id;
        string code;
        string subject;
        bool retired;
    }
    mapping (uint => Subject) public subjects; // mapping the subject to subjects using key _id
    // function createsubject to add the subjects
    function createSubject(string memory _code, string memory _subject ) public returns (Subject memory){
        subjectsCount++;
        subjects[subjectsCount] = Subject(subjectsCount, _code, _subject, false);
        emit createSubjectEvent(subjectsCount, _code, _subject, false);
        return subjects[subjectsCount];
    }
    event createSubjectEvent(
        uint _id,
        string indexed code,
        string subject,
        bool retired
    );
    event markRetiredEvent(
        uint indexed _id
    );

    function markRetired(uint _id) public returns (Subject memory) {
        subjects[_id].retired = true;
        emit markRetiredEvent(_id);
        return subjects[_id];
    }
    function findSubject(uint _id) public view returns(Subject memory) {
        return subjects[_id];
    }

    function updatedSubject(uint id, string memory newcode, string  memory newsubject) public returns (Subject memory) {
        subjects[id].code = newcode;
        subjects[id].subject = newsubject;
        emit updateSubjectEvent(newcode, newsubject);
        return subjects[id];   
    }

    event updateSubjectEvent(
        string indexed code,
        string subject

    );
}