import React, { Component } from 'react';

class FindMarks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      marks: '',
    };
  }

  render() {
    const { students = [], subjects = [] } = this.props; // Set default values for students and subjects

    return (
      <form
        className="p-4"
        style={{
          border: '1px solid black',
          borderRadius: '20px',
          padding: '20px',
          margin: '20px auto',
          width: '100%',
        }}
        onSubmit={(event) => {
          event.preventDefault();
          this.props
            .findMarks(this.studentid.value, this.subjectid.value)
            .then((result) => {
              this.setState({ marks: result.grades });
            });
        }}
      >
        <h2>Find Marks</h2>
        <select
          id="studentid"
          className="form-select mb-2"
          style={{
            padding: '10px',
            margin: '10px auto',
            width: '100%',
            borderRadius: '10px',
          }}
          ref={(input) => {
            this.studentid = input;
          }}
        >
          <option defaultValue={true}>Select the student</option>
          {students.map((student, key) => (
            <option value={student.cid} key={key}>
              {student.cid} {student.name}
            </option>
          ))}
        </select>
        <select
          id="subjectid"
          className="form-select mb-2"
          style={{
            padding: '10px',
            margin: '10px auto',
            width: '100%',
            borderRadius: '10px',
          }}
          ref={(input) => {
            this.subjectid = input;
          }}
        >
          <option defaultValue={true}>Select the subject</option>
          {subjects.map((subject, key) => (
            <option value={subject.code} key={key}>
              {subject.code} {subject.name}
            </option>
          ))}
        </select>
        <input className="form-control btn btn-primary" type="submit" hidden="" />
        <p>Grade: {this.state.marks}</p>
      </form>
    );
  }
}

export default FindMarks;
