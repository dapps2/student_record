import React, { useState } from 'react';

function SubjectList(props) {
  const [formData, setFormData] = useState({
    code: '',
    subject: '',
  });

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const { code, subject } = formData;
    if (code === '' || subject === '') {
      alert('Code and subject cannot be empty');
    } else {
      props.createSubject(code, subject);
      setFormData({
        code: '',
        subject: '',
      });
    }
  };

  const handleUpdateSubject = (e) => {
    e.preventDefault();
    const subjectObj = props.subjectObj;
    if (subjectObj && subjectObj._id) {
      props.updateSubject(subjectObj._id, formData.subject, formData.code);
      setFormData({
        code: '',
        subject: '',
      });
    }
  };

  return (
    <form
      style={{
        border: '1px solid black',
        borderRadius: '20px',
        padding: '20px',
        margin: '20px auto',
        width: '100%',
      }}
    >
      {props.update ? (
        <div>
          <div className="form-group">
            <label htmlFor="code">Code</label>
            <input
              type="text"
              placeholder="Code"
              className="form-control"
              id="code"
              name="code"
              value={props.subjectObj && props.subjectObj.code ? props.subjectObj.code : ''}
              readOnly
            />
          </div>
          <div className="form-group">
            <label htmlFor="subject">Subject</label>
            <input
              type="text"
              placeholder="Subject"
              className="form-control"
              id="subject"
              name="subject"
              value={formData.subject}
              onChange={handleChange}
            />
          </div>
          <button
            onClick={handleUpdateSubject}
            type="submit"
            className="btn btn-success"
            style={{ marginTop: '10px', width:"100%" }}
          >
            Update
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="code">Code</label>
            <input
              type="text"
              placeholder="Code"
              className="form-control"
              id="code"
              name="code"
              value={formData.code}
              onChange={handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="subject">Subject</label>
            <input
              type="text"
              placeholder="Subject"
              className="form-control"
              id="subject"
              name="subject"
              value={formData.subject}
              onChange={handleChange}
            />
          </div>
          <button
            onClick={handleSubmit}
            type="submit"
            className="btn btn-success"
            style={{ marginTop: '10px', width: "100%" }}
          >
            Submit
          </button>
        </div>
      )}
    </form>
  );
}

export default SubjectList;
