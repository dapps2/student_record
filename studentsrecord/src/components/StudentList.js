import React, { useState } from 'react';

function StudentList(props) {
  const [formData, setFormData] = useState({
    cid: '',
    name: '',
  });

  const handleChange = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const { cid, name } = formData;
    if (cid === '' || name === '') {
      alert('cid and name cannot be empty');
    } else {
      props.createStudent(cid, name);
      setFormData({
        cid: '',
        name: '',
      });
    }
  };

  const handleUpdateName = (e) => {
    e.preventDefault();
    const studentId = props.studentObj && props.studentObj._id;
    if (studentId) {
      props.updateName(studentId, formData.name);
      setFormData({
        cid: '',
        name: '',
      });
    }
  };

  return (
    <form style={{ border: '1px solid black', borderRadius: '20px', padding: '20px', margin: '20px auto', width: '100%' }}>
      {props.update && props.studentObj ? (
        <div>
          <div className="form-group">
            <label htmlFor="cid">CID</label>
            <input
              type="text"
              placeholder="CID"
              className="form-control"
              id="cid"
              name="cid"
              value={props.studentObj.cid || ''}
              disabled
            />
          </div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              placeholder="NAME"
              className="form-control"
              id="name"
              name="name"
              value={formData.name}
              onChange={handleChange}
            />
          </div>
          <button onClick={handleUpdateName} type="submit" className="btn btn-success" style={{ marginTop: '10px', width:"100%" }}>
            Update
          </button>
        </div>
      ) : (
        <div>
          <div className="form-group">
            <label htmlFor="cid">CID</label>
            <input
              type="text"
              placeholder="CID"
              className="form-control"
              id="cid"
              name="cid"
              value={formData.cid}
              onChange={handleChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="name">Name</label>
            <input
              type="text"
              placeholder="NAME"
              className="form-control"
              id="name"
              name="name"
              value={formData.name}
              onChange={handleChange}
            />
          </div>
          <button onClick={handleSubmit} type="submit" className="btn btn-success" style={{ marginTop: '10px', width: "100%" }}>
            Submit
          </button>
        </div>
      )}
    </form>
  );
}

export default StudentList;
